import { Icon } from "@mdi/react";
import { mdiCheckBold, mdiCloseThick } from "@mdi/js";
import { PropsWithChildren, useContext, useState } from "react";
import React from "react";

export interface AlertProps {
    type: 'success' | 'error';
    message: string;
}

export interface AlertContextProps {
    props: AlertProps | null;
    alert: (props: AlertProps | null) => void;
}

const AlertContext = React.createContext<AlertContextProps>(null!);

export function useAlert() {
    return useContext(AlertContext);    
}

export function Alert(props: AlertProps) {
    return <div className="bg-white text-cyan-900 px-5 py-4 rounded-2xl flex gap-3 justify-between max-w-[280px] animate-slide">
        <Icon path={props.type === 'error' ? mdiCloseThick : mdiCheckBold} color={props.type === 'error' ? 'red' : 'green'} size={1}/>
        <p>{props.message}</p>
    </div>
}

export function AlertProvider(props: PropsWithChildren<{}>) {
    const [alertProps, setAlertProps] = useState<AlertProps | null>(null);

    const alert = (props: AlertProps | null) => {
        setAlertProps(props);
        setTimeout(() => setAlertProps(null), 2000);
    }

    return <AlertContext.Provider value={{props: alertProps, alert}}>
        {props.children}
        <div className="absolute bottom-0 py-8 w-full flex flex-col-reverse items-center overflow-hidden">
            {alertProps && <Alert {...alertProps}/>}
        </div>
    </AlertContext.Provider>
}