/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{ts,tsx}",
  ],
  theme: {
    extend: {
      keyframes: {
        'slide': {
          '0%': { transform: 'translateY(200%)' },
          '100%': { transform: 'translateY(0%)' }
        }
      },
      animation: {
        'slide': 'slide 350ms ease-out'
      }
    },
  },
  plugins: [],
}
