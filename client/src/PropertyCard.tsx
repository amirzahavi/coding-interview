import { Property } from "./Property";
import classNames from 'classnames';
import { formatPrice } from "./utils";

interface PropertyCardProps {
    property: Property;
    selected: boolean;
    onClick: () => void;
}

export function PropertyCard({property, selected, onClick}: PropertyCardProps) {
    const classes = classNames('bg-cyan-800 rounded p-5 cursor-pointer shadow-sm hover:shadow-2xl', {'shadow-2xl border-slate-500 border-solid border-2': selected});    

    return <div className={classes} onClick={onClick}>
        <h3 className="bg-cyan-900 rounded text-center py-1">{property.title}</h3>
        <div className="pt-3 flex flex-col gap-2">            
            <p className="text-sm text-slate-300">{property.address.state}, {property.address.city}</p>
            <p className="text-sm text-slate-300">{property.address.street} / {property.address.number}</p>
            <span className="self-end bg-slate-100 text-cyan-800 rounded px-3 py-1">{formatPrice(Number(property.price))} $</span>
        </div>
    </div>
}