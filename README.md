# Imagene AI coding interview

> The goal of this coding interview assignment is to get a feeling of the candidate coding skills and clean code, as well as learning new technologies and how the candidate approach a problem

## Introduction

This project is a Real Estate agency dashboard for view and mange their clients properties.

## Usage

### Server

install and run server

```sh
cd server

npm i

npm run start:dev
```

### Client

install and run client

```sh
cd client

npm i

npm run dev
```

## Assignment

complete the `update` property functionality (incl. API validation - check that `number_of_rooms` is between `1` to `10`)

Extra:

add functionality to `create` a new property

