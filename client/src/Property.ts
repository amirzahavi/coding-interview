export interface Property {
    id:              number;
    title:       string;
    number_of_rooms: number;
    price:        number;
    floor:         number;
    contact:        string;
    addressId?: number;
    address:        Address;    
  }
  
  export interface Address {
    state:    string;
    city:     string;
    street:   string;
    number:   number;    
  }
  