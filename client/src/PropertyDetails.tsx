import React, { Ref, useEffect } from "react";
import { Property } from "./Property";
import { useForm, UseFormRegister } from "react-hook-form";
import axios from "axios";
import { useAlert } from "./Alert";

interface PropertyDetailsProps {
    property: Property;
}

export function PropertyDetails({property}: PropertyDetailsProps) {
    const { register, handleSubmit, reset } = useForm({defaultValues: property});
    const {alert} = useAlert();

    useEffect(() => reset(property), [property]);

    const onSubmit = async (data: Property) => {
        try {
            const {id, ...body} = data;
            const response = await axios.put(`${import.meta.env.VITE_SERVER_HOST}/${id}`, body);
            reset(response.data);
            alert({type: 'success', message: 'property successfully updated'});
        } catch (e) {
            reset();
            alert({type: 'error', message: 'failed to update property'});
        }
    };

    return <form onSubmit={handleSubmit(onSubmit)} className="px-32 py-8 rounded-lg bg-cyan-700 shadow shadow-cyan-500 flex flex-col gap-4">
        <PropertyField {...register("title")} label="Title"/>
        <PropertyField {...register("price", {valueAsNumber: true})} label="Price"/>
        <PropertyField {...register("number_of_rooms", {valueAsNumber: true})} label="Number of Rooms" type="number"/>
        <PropertyField {...register("floor", {valueAsNumber: true})} label="Floor" type="number"/>
        <PropertyField {...register("contact")} label="Contact"/>        
        <fieldset title="Address" className="border border-cyan-900 flex flex-col gap-4 p-5">
            <legend className="text-cyan-900 text-sm px-5">Address</legend>
            <PropertyField label="State" {...register("address.state")}/>
            <PropertyField label="City" {...register("address.city")}/>            
            <PropertyField label="Street" {...register("address.street")}/>
            <PropertyField label="Number" {...register("address.number", {valueAsNumber: true})} type="number"/>
        </fieldset>
        <input type="submit" className="transition border-2 border-cyan-900 rounded-3xl w-32 self-center max-w-xs py-1 cursor-pointer shadow-md hover:bg-cyan-900 focus:bg-cyan-900 focus:shadow-lg hover:shadow-lg active:shadow-none"/>
    </form>
}

interface PropertyFieldProps extends ReturnType<UseFormRegister<Property>>{
    label: string;
    type?: HTMLInputElement["type"];
}

const PropertyField = React.forwardRef(function PropertyField(props: PropertyFieldProps, ref: Ref<HTMLInputElement>) {
    const {label, type, ...inputProps} = props;
    return  (<div className="flex gap-5 justify-between">
        <label>{label}</label>
        <input {...inputProps} type={props.type ?? 'text'} className="text-cyan-900 px-3 py-1 rounded-2xl outline-cyan-900" ref={ref}/>
    </div> )
})