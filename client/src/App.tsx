import { useEffect, useState } from 'react'
import {Icon} from '@mdi/react'
import { mdiHomeVariantOutline } from "@mdi/js";
import { PropertyDetails } from './PropertyDetails';
import { PropertyCard } from './PropertyCard';
import { Property } from './Property';
import axios from 'axios';
import { AlertProvider } from './Alert';

function App() {
  const [properties, setProperty] = useState<Property[]>([]);
  const [selected, setSelected] = useState(0);

  useEffect(() => {
    axios.get(`${import.meta.env.VITE_SERVER_HOST}/`).then(({data}) => setProperty(data));
  }, []);  

    return (
      <AlertProvider>
      <div className="bg-cyan-900 text-white font-sans grid grid-cols-[1fr_2fr]">        
        <h1 className='bg-cyan-700 text-white text-6xl text-center col-span-2 py-6 h-28'>
          <Icon path={mdiHomeVariantOutline} size={2} className="inline mr-4"/>
          <span>Imagene Real Estate</span>
        </h1>
        <aside className='flex flex-col gap-4 px-8 py-5 overflow-y-scroll max-h-header'>
          <h2 className='text-3xl mb-5 text-center'>Available Properties</h2>
          {
            properties.map((property, i) => <PropertyCard key={property.id} property={property} selected={i === selected} onClick={() => setSelected(i)}/>)
          }
        </aside>
        <main className='w-full grid place-items-center'>{
          properties[selected] && <PropertyDetails property={properties[selected]}/>
        }</main>
      </div>
    </AlertProvider>      
  )
}

export default App
