import { Injectable } from '@nestjs/common';
import { PrismaService } from './prisma.service';

@Injectable()
export class AppService {
  constructor(private readonly prisma: PrismaService) {}

  async getAllProperties() {
    return this.prisma.property.findMany({
      select: {
        id: true,
        address: true,
        contact: true,
        floor: true,
        number_of_rooms: true,
        price: true,
        title: true,
      },
    });
  }
}
