export class UpdatePropertyDto {
  title: string;
  number_of_rooms: number;
  price: number;
  floor: number;
  contact: string;
  address: UpdateAddressDto;
}

export class UpdateAddressDto {
  id: number;
  state: string;
  city: string;
  street: string;
  number: number;
}
