export const formatPrice = (price: number) => price.toFixed(3);
